﻿using System;
using UnityEngine;


namespace YOZH.Core.Misc
{
	public static class Easing
	{

		public static float GetValue(float startValue,
										   float targetValue,
										   float startTime,
										   float duration,
										   float currentTime,
										   Type easingType)
		{
			return getValue(startValue, targetValue, currentTime - startTime, duration, easingType);
		}
		public static Vector3 GetValue(Vector3 startValue,
											 Vector3 targetValue,
											 float startTime,
											 float duration,
											 float currentTime,
											 Type easingType)
		{
			Vector3 result = startValue;
			float easing = GetValue(0,
											 Vector3.Distance(startValue, targetValue),
											 startTime,
											 duration,
											 currentTime,
											 easingType);

			Vector3 direction = (targetValue - startValue).normalized;
			result.x += easing * direction.x;
			result.y += easing * direction.y;
			result.z += easing * direction.z;

			return result;
		}

		private static float getValue(float startValue,
											float targetValue,
											float currentTime,
											float duration,
											Type easingType)
		{
			switch (easingType) {
				case (Type.EaseInQuad):
					return easeInQuad(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutQuad):
					return easeOutQuad(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutQuad):
					return easeInOutQuad(currentTime, startValue, targetValue, duration);
				case (Type.EaseInCubic):
					return easeInCubic(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutCubic):
					return easeOutCubic(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutCubic):
					return easeInOutCubic(currentTime, startValue, targetValue, duration);
				case (Type.EaseInQuart):
					return easeInQuart(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutQuart):
					return easeOutQuart(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutQuart):
					return easeInOutQuart(currentTime, startValue, targetValue, duration);
				case (Type.EaseInQuint):
					return easeInQuint(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutQuint):
					return easeOutQuint(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutQuint):
					return easeInOutQuint(currentTime, startValue, targetValue, duration);
				case (Type.EaseInSine):
					return easeInSine(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutSine):
					return easeOutSine(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutSine):
					return easeInOutSine(currentTime, startValue, targetValue, duration);
				case (Type.EaseInExpo):
					return easeInExpo(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutExpo):
					return easeOutExpo(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutExpo):
					return easeInOutExpo(currentTime, startValue, targetValue, duration);
				case (Type.EaseInCirc):
					return easeInCirc(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutCirc):
					return easeOutCirc(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutCirc):
					return easeInOutCirc(currentTime, startValue, targetValue, duration);
				case (Type.EaseInElastic):
					return easeInElastic(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutElastic):
					return easeOutElastic(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutElastic):
					return easeInOutElastic(currentTime, startValue, targetValue, duration);
				case (Type.EaseInBack):
					return easeInBack(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutBack):
					return easeOutBack(currentTime, startValue, targetValue, duration);
				case (Type.EaseInOutBack):
					return easeInOutBack(currentTime, startValue, targetValue, duration);
				case (Type.EaseInBounce):
					return easeInBounce(currentTime, startValue, targetValue, duration);
				case (Type.EaseOutBounce):
					return easeOutBounce(currentTime, startValue, targetValue, duration);
				default: {
						throw new NotImplementedException("[Easing] Calculate of: " + easingType + " is non implemented");
					}
			}
		}

		private static float easeInQuad(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * (currentTime /= duration) * currentTime + startValue;
		}
		private static float easeOutQuad(float currentTime, float startValue, float targetValue, float duration)
		{
			return -targetValue * (currentTime /= duration) * (currentTime - 2) + startValue;
		}
		private static float easeInOutQuad(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration / 2) < 1) return targetValue / 2 * currentTime * currentTime + startValue;
			return -targetValue / 2 * ((--currentTime) * (currentTime - 2) - 1) + startValue;
		}
		private static float easeInCubic(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * (currentTime /= duration) * currentTime * currentTime + startValue;
		}
		private static float easeOutCubic(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * ((currentTime = currentTime / duration - 1) * currentTime * currentTime + 1) + startValue;
		}
		private static float easeInOutCubic(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration / 2) < 1) {
				return targetValue / 2 * currentTime * currentTime * currentTime + startValue;
			}
			return targetValue / 2 * ((currentTime -= 2) * currentTime * currentTime + 2) + startValue;
		}
		private static float easeInQuart(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * (currentTime /= duration) * currentTime * currentTime * currentTime + startValue;
		}
		private static float easeOutQuart(float currentTime, float startValue, float targetValue, float duration)
		{
			return -targetValue * ((currentTime = currentTime / duration - 1) * currentTime * currentTime * currentTime - 1) + startValue;
		}
		private static float easeInOutQuart(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration / 2) < 1) return targetValue / 2 * currentTime * currentTime * currentTime * currentTime + startValue;
			return -targetValue / 2 * ((currentTime -= 2) * currentTime * currentTime * currentTime - 2) + startValue;
		}
		private static float easeInQuint(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * (currentTime /= duration) * currentTime * currentTime * currentTime * currentTime + startValue;
		}
		private static float easeOutQuint(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue * ((currentTime = currentTime / duration - 1) * currentTime * currentTime * currentTime * currentTime + 1) + startValue;
		}
		private static float easeInOutQuint(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration / 2) < 1) return targetValue / 2 * currentTime * currentTime * currentTime * currentTime * currentTime + startValue;
			return targetValue / 2 * ((currentTime -= 2) * currentTime * currentTime * currentTime * currentTime + 2) + startValue;
		}
		private static float easeInSine(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)(-targetValue * Math.Cos(currentTime / duration * (Math.PI / 2)) + targetValue + startValue);
		}
		private static float easeOutSine(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)(targetValue * Math.Sin(currentTime / duration * (Math.PI / 2)) + startValue);
		}
		private static float easeInOutSine(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)(-targetValue / 2 * (Math.Cos(Math.PI * currentTime / duration) - 1) + startValue);
		}
		private static float easeInExpo(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)((currentTime == 0) ? startValue : targetValue * Math.Pow(2,
																							 10 * (currentTime / duration - 1)) + startValue);
		}
		private static float easeOutExpo(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)((currentTime == duration) ? startValue + targetValue : targetValue * (-Math.Pow(2,
																													-10 * currentTime / duration) + 1) + startValue);
		}
		private static float easeInOutExpo(float currentTime, float startValue, float targetValue, float duration)
		{
			if (currentTime == 0) return startValue;
			if (currentTime == duration) return startValue + targetValue;
			if ((currentTime /= duration / 2) < 1) return (float)(targetValue / 2 * Math.Pow(2, 10 * (currentTime - 1)) + startValue);
			return (float)(targetValue / 2 * (-Math.Pow(2, -10 * --currentTime) + 2) + startValue);
		}
		private static float easeInCirc(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)(-targetValue * (Math.Sqrt(1 - (currentTime /= duration) * currentTime) - 1) + startValue);
		}
		private static float easeOutCirc(float currentTime, float startValue, float targetValue, float duration)
		{
			return (float)(targetValue * Math.Sqrt(1 - (currentTime = currentTime / duration - 1) * currentTime) + startValue);
		}
		private static float easeInOutCirc(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration / 2) < 1) return (float)(-targetValue / 2 * (Math.Sqrt(1 - currentTime * currentTime) - 1) + startValue);
			return (float)(targetValue / 2 * (Math.Sqrt(1 - (currentTime -= 2) * currentTime) + 1) + startValue);
		}
		private static float easeInElastic(float currentTime, float startValue, float targetValue, float duration)
		{
			float s = 1.70158f;
			float p = 0;
			float a = targetValue;
			if (currentTime == 0) return startValue;
			if ((currentTime /= duration) == 1) return startValue + targetValue;
			if (p == 0) p = duration * 0.3f;
			if (a < Math.Abs(targetValue)) {
				a = targetValue;
				s = p / 4;
			}
			else s = (float)(p / (2 * Math.PI) * Math.Asin(targetValue / a));
			return -(float)(a * Math.Pow(2, 10 * (currentTime -= 1)) * Math.Sin((currentTime * duration - s) * (2 * Math.PI) / p)) + startValue;
		}
		private static float easeOutElastic(float currentTime, float startValue, float targetValue, float duration)
		{
			var s = 1.70158f;
			float p = 0;
			float a = targetValue;
			if (currentTime == 0) return startValue;
			if ((currentTime /= duration) == 1) return startValue + targetValue;
			if (p == 0) p = duration * 0.3f;
			if (a < Math.Abs(targetValue)) {
				a = targetValue;
				s = p / 4;
			}
			else s = (float)(p / (2 * Math.PI) * Math.Asin(targetValue / a));
			return (float)(a * Math.Pow(2, -10 * currentTime) * Math.Sin((currentTime * duration - s) * (2 * Math.PI) / p) + targetValue + startValue);
		}
		private static float easeInOutElastic(float currentTime, float startValue, float targetValue, float duration)
		{
			float s = 1.70158f;
			float p = 0;
			float a = targetValue;

			if (currentTime == 0) return startValue;
			if ((currentTime /= duration / 2) == 2) return startValue + targetValue;
			if (p == 0) p = duration * (0.3f * 1.5f);
			if (a < Math.Abs(targetValue)) {
				a = targetValue;
				s = p / 4;
			}
			else s = (float)(p / (2 * Math.PI) * Math.Asin(targetValue / a));
			if (currentTime < 1) return -(float)(0.5f * (a * Math.Pow(2, 10 * (currentTime -= 1)) * Math.Sin((currentTime * duration - s) * (2 * Math.PI) / p)) + startValue);
			return (float)(a * Math.Pow(2, -10 * (currentTime -= 1)) * Math.Sin((currentTime * duration - s) * (2 * Math.PI) / p) * .5 + targetValue + startValue);
		}
		private static float easeInBack(float currentTime, float startValue, float targetValue, float duration)
		{
			float s = 1.70158f;
			return targetValue * (currentTime /= duration) * currentTime * ((s + 1) * currentTime - s) + startValue;
		}
		private static float easeOutBack(float currentTime, float startValue, float targetValue, float duration)
		{
			float s = 1.70158f;
			return targetValue * ((currentTime = currentTime / duration - 1) * currentTime * ((s + 1) * currentTime + s) + 1) + startValue;
		}
		private static float easeInOutBack(float currentTime, float startValue, float targetValue, float duration)
		{
			float s = 1.70158f;
			if ((currentTime /= duration / 2) < 1) return targetValue / 2 * (currentTime * currentTime * (((s * (1.525f)) + 1) * currentTime - s)) + startValue;
			return targetValue / 2 * ((currentTime -= 2) * currentTime * (((s * (1.525f)) + 1) * currentTime + s) + 2) + startValue;
		}
		private static float easeInBounce(float currentTime, float startValue, float targetValue, float duration)
		{
			return targetValue - easeOutBounce(duration - currentTime, 0, targetValue, duration) + startValue;
		}
		private static float easeOutBounce(float currentTime, float startValue, float targetValue, float duration)
		{
			if ((currentTime /= duration) < (1 / 2.75)) {
				return targetValue * (7.5625f * currentTime * currentTime) + startValue;
			}
			else if (currentTime < (2 / 2.75)) {
				return (float)(targetValue * (7.5625 * (currentTime - (1.5f / 2.75)) * currentTime + .75) + startValue);
			}
			else if (currentTime < (2.5 / 2.75)) {
				return (float)(targetValue * (7.5625 * (currentTime - (2.25 / 2.75)) * currentTime + .9375) + startValue);
			}
			else {
				return (float)(targetValue * (7.5625 * (currentTime - (2.625 / 2.75)) * currentTime + .984375) + startValue);
			}
		}

		public enum Type
		{
			EaseInQuad = 0,
			EaseOutQuad = 1,
			EaseInOutQuad = 2,
			EaseInCubic = 3,
			EaseOutCubic = 4,
			EaseInOutCubic = 5,
			EaseInQuart = 6,
			EaseOutQuart = 7,
			EaseInOutQuart = 8,
			EaseInQuint = 9,
			EaseOutQuint = 10,
			EaseInOutQuint = 11,
			EaseInSine = 12,
			EaseOutSine = 13,
			EaseInOutSine = 14,
			EaseInExpo = 15,
			EaseOutExpo = 16,
			EaseInOutExpo = 17,
			EaseInCirc = 18,
			EaseOutCirc = 19,
			EaseInOutCirc = 20,
			EaseInElastic = 21,
			EaseOutElastic = 22,
			EaseInOutElastic = 23,
			EaseInBack = 24,
			EaseOutBack = 25,
			EaseInOutBack = 26,
			EaseInBounce = 27,
			EaseOutBounce = 28
		}
	}
}