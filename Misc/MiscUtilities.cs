﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.EventSystems;
using System;
using System.Security.Cryptography;
using System.Text;

namespace YOZH.Core.Misc
{
	public static class MiscUtilities
	{
		/// <summary>
		/// Normalize file path from string.
		/// </summary>
		/// <param name="path">Source path</param>
		/// <param name="relativeTo"> </param>
		/// <param name="fileExtention"></param>
		/// <returns></returns>
		public static string NormalizePath(string path, string relativeTo = "", string fileExtention = "")
		{
			string result = string.Empty;
			string[] pathArr = path.Split(@"/\".ToCharArray());
			foreach (string dir in pathArr) if (!string.IsNullOrEmpty(dir)) result += string.Format("{0}/", dir);

			result = result.TrimEnd(@"/".ToCharArray());
			if (!string.IsNullOrEmpty(relativeTo) && !result.StartsWith(relativeTo)) result = string.Format("{0}/{1}", relativeTo, result);

			if (!string.IsNullOrEmpty(fileExtention)) {
				fileExtention = string.Format(".{0}", fileExtention.TrimStart(@".".ToCharArray()));

				string currentExt = Path.GetExtension(result);
				if (string.IsNullOrEmpty(currentExt)) result = string.Format(@"{0}{1}", result, fileExtention);
				else result = result.Replace(currentExt, fileExtention);

				if (!result.EndsWith(fileExtention)) result = string.Format(@"{0}{1}", result, fileExtention);
			}
			return result;
		}

		public static bool GetCastedComponent<T>(Vector2 screenPosition, out T result, bool ignoreUI = false)
			where T : Component
		{
			Ray ray = Camera.main.ScreenPointToRay(screenPosition);
			RaycastHit hit;

			if ((ignoreUI || !EventSystem.current.IsPointerOverGameObject()) && Physics.Raycast(ray, out hit)) {
				result = hit.collider.gameObject.GetComponentInChildren<T>();
				return result != null;
			}
			result = null;
			return false;
		}
		public static T GetCastedComponent<T>(Vector2 screenPosition, bool ignoreUI = false) where T : Component
		{
			T result;
			GetCastedComponent<T>(screenPosition, out result, ignoreUI);
			return result;
		}
		public static IEnumerator DeferredAction(float delay, Action action)
		{
			yield return new WaitForSeconds(delay);
			action();
		}

		public static string GetUniqueID(int lenth = 8,
										 string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
										 string prefix = "",
										 string postfix = "")
		{
			RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
			byte[] data = new byte[lenth];
			crypto.GetNonZeroBytes(data);
			StringBuilder result = new StringBuilder(lenth);
			foreach (byte b in data) {
				result.Append(chars[b % (chars.Length - 1)]);
			}
			return String.Format("{0}{1}{2}", prefix, result, postfix);
		}
	}
}