﻿using System;
using UnityEngine;

namespace YOZH.Core.GUIDLinks
{
	[Serializable]
	public class ScriptableObjectGUID<T> : GUIDLink<T> where T : ScriptableObject
	{
		protected override T getInstance()
		{
			return Resources.Load<T>(loadPath);
		}
	}
}