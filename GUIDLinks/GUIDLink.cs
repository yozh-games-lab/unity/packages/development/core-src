﻿using UnityEngine;
using System;
using System.IO;
using YOZH.Core.Misc;
using YOZH.Core.APIExtentions.Unity;

namespace YOZH.Core.GUIDLinks
{
	public abstract class GUIDLink<T>
	{
#pragma warning disable 0414
		[SerializeField]
		private string guid = "";
#pragma warning restore 0414

		[SerializeField]
		private string assetPath = "";

		public T Instance {
			get {
				if (getInstance() == null) {
					throw new NullReferenceException(string.Format("Can't load asset from path [{0}]", assetPath));
				}
				return getInstance();
			}
		}

		protected abstract T getInstance();

		protected string loadPath { get { return AssetDatabaseUtility.GetResourceLoadPath(assetPath); } }

		public static bool IsCorrectAssetPath(string assetPath)
		{
			return assetPath.StartsWith("Assets/") && assetPath.Contains("/Resources/");
		}
	}
}