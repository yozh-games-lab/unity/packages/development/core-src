﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YOZH.Core.Misc;

namespace YOZH.Core.GUIDLinks
{
	[Serializable]
	public class ComponentGUID<T> : GUIDLink<T> where T : Component
	{
		protected override T getInstance()
		{
			GameObject obj = Resources.Load<GameObject>(loadPath);
			return (obj != null) ? obj.GetComponent<T>() : default(T);
		}
	}
}