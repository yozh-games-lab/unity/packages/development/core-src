﻿using UnityEngine;

namespace YOZH.Core.GUIDLinks.Editor
{
	public abstract class ScriptableObjectGUIDDrawer<T> : GUIDLinkPropertyDrawer<T> where T : ScriptableObject
	{
	}
}