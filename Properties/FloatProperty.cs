﻿using System;
using UnityEngine.Events;

namespace YOZH.Core.Properties
{
	[Serializable]
	public class FloatProperty : BaseProperty<float, FloatProperty.ChangeEvent>
	{
		public FloatProperty() : base()
		{
		}

		public FloatProperty(float value) : base(value)
		{
		}

		[Serializable]
		public class ChangeEvent : UnityEvent<float, float>
		{
		}
	}
}