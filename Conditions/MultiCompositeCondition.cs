﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using YOZH.Core.CustomAttributes;

namespace YOZH.Core.Conditions
{
	public abstract class MultiCompositeCondition : CompositeCondition
	{
		[SerializeField, InterfaceField(typeof(ICondition), allowSceneObject: true)]
		private List<MonoBehaviour> SubConditionBehaviours = new List<MonoBehaviour>();

		public override bool IsTrue()
		{
			return IsTrue(GetValidSubConditions());
		}

		protected override IEnumerable<ICondition> GetValidSubConditions()
		{
			return SubConditionBehaviours.Where(item => item != null).Select(item => item as ICondition);
		}

		protected abstract bool IsTrue(IEnumerable<ICondition> subConditions);
	}
}