﻿using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.Conditions.Comparatos
{
	public abstract class ComparatorConditionBase : MonoBehaviour, ICondition
	{
		bool ICondition.IsTrue() => IsTrue();
		protected abstract bool IsTrue();
	}

	public abstract class ComparatorCondition<TArgument> : ComparatorConditionBase
	{
		[SerializeField] private TArgument Argument1;
		[SerializeField] private TArgument Argument2;
		[SerializeField] private UnityEvent OnTrueEvent;
		[SerializeField] private UnityEvent OnFalseEvent;

		public void SetArgument1(TArgument value)
		{
			Argument1 = value;
		}

		public void SetArgument2(TArgument value)
		{
			Argument2 = value;
		}

		public void Compare()
		{
			UnityEvent result = Compare(Argument1, Argument2) ? OnTrueEvent : OnFalseEvent;
			result?.Invoke();
		}

		protected abstract bool Compare(TArgument arg1, TArgument arg2);

		protected sealed override bool IsTrue()
		{
			return Compare(Argument1, Argument2);
		}
	}
}