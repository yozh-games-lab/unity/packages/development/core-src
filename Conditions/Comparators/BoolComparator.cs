﻿namespace YOZH.Core.Conditions.Comparatos
{
	public class BoolComparator : ComparatorCondition<bool>
	{
		protected override bool Compare(bool arg1, bool arg2) => arg1 == arg2;
	}
}