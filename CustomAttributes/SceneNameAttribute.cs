﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;


namespace YOZH.Core.CustomAttributes
{
	public class SceneNameAttribute : PopupAttribute
	{
		public SceneNameAttribute()
		{
#if UNITY_EDITOR
			List<string> choises = new List<string>();

			foreach (UnityEditor.EditorBuildSettingsScene scene in UnityEditor.EditorBuildSettings.scenes) {
				if (scene.enabled) {
					choises.Add(Path.GetFileNameWithoutExtension(scene.path));
				}
			}
			Choices = choises.ToArray();
#endif
		}
	}
}