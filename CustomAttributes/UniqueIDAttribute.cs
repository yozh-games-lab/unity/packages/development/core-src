﻿using UnityEngine;
using System.Collections;


namespace YOZH.Core.CustomAttributes
{
	public class UniqueIDAttribute : PropertyAttribute
	{
		private int lenth;
		private string chars;
		private string prefix;
		private string postfix;

		public UniqueIDAttribute(int lenth = 8, string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", string prefix = "", string postfix = "")
		{
			this.lenth = lenth;
			this.chars = chars;
			this.prefix = prefix;
			this.postfix = postfix;
		}

		public string GenerateID()
		{
			return Misc.MiscUtilities.GetUniqueID(lenth, chars, prefix, postfix);
		}
	}
}