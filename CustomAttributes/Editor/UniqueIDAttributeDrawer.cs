﻿using UnityEngine;
using UnityEditor;

namespace YOZH.Core.CustomAttributes.Editor
{
	[CustomPropertyDrawer(typeof(UniqueIDAttribute))]
	public class UniqueIDAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (string.IsNullOrEmpty(property.stringValue)) property.stringValue = ((UniqueIDAttribute)attribute).GenerateID();
			EditorGUI.PropertyField(position, property, label, true);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property);
		}
	}
}