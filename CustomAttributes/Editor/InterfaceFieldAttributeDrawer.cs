﻿using UnityEngine;
using UnityEditor;
using YOZH.Core.APIExtentions.Unity.Editor;
using System;

namespace YOZH.Core.CustomAttributes.Editor
{
	[CustomPropertyDrawer(typeof(InterfaceFieldAttribute))]
	public class InterfaceFieldAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			InterfaceFieldAttribute attr = (InterfaceFieldAttribute)attribute;
			Type fieldType = property.FieldType(fieldInfo.FieldType);

			property.objectReferenceValue = EditorGUIUtilityExt.InterfaceField(position, label, property.objectReferenceValue, fieldType, attr.InterfaceType, attr.AllowSceneObject);
		}
	}
}