﻿using System.Collections.Generic;
using System;
using YOZH.Core.APIExtentions.Unity;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace YOZH.Core.CustomAttributes
{
	public class PrefabPathAttribute : PopupAttribute
	{
		public PrefabPathAttribute(Type type, params string[] nameFilters)
		{
#if UNITY_EDITOR
			List<string> result = new List<string>();

			List<string> GUIDList = new List<string>();
			foreach (string filter in nameFilters) {
				string[] filteredList = AssetDatabase.FindAssets(string.Format("{0} t:Prefab", filter));

				foreach (string filtered in filteredList) {
					if (!GUIDList.Contains(filtered)) GUIDList.Add(filtered);
				}
			}

			foreach (string GUID in GUIDList) {
				string path = AssetDatabase.GUIDToAssetPath(GUID);
				UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(path, type);

				if (obj != null && path.Contains(@"/Resources/")) {
					string resourcePath = AssetDatabaseUtility.GetResourceLoadPath(path);

					if (!string.IsNullOrEmpty(resourcePath)) result.Add(resourcePath);
				}
			}

			Choices = result.ToArray();
#endif
		}
		public PrefabPathAttribute(Type type) : this(type, "")
		{
		}
	}
}