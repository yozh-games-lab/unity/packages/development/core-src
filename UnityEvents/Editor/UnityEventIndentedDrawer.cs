﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace YOZH.Core.UnityEvents.Editor
{
	public abstract class UnityEventIndentedDrawer : UnityEventDrawer
	{
		public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
		{
			base.OnGUI(EditorGUI.IndentedRect(position), property, label);
		}
	}
}