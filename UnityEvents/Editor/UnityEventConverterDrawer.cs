﻿using UnityEditor;
using YOZH.Core.APIExtentions.Unity.Editor;

namespace YOZH.Core.UnityEvents.Editor
{
	[CustomEditor(typeof(BaseUnityEventCoverter), true)]
	public class UnityEventConverterDrawer : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			this.DrawDefaultScriptTitle(serializedObject);

			SerializedProperty outputEventProperty = serializedObject.FindProperty("OnOutputEvent");
			DrawPropertiesExcluding(serializedObject, EditorDrawerUtility.ScriptFieldName, outputEventProperty.name);
			EditorGUILayout.Space();
			EditorGUILayout.PropertyField(outputEventProperty);

			serializedObject.ApplyModifiedProperties();
		}
	}
}