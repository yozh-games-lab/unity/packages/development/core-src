﻿using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.UnityEvents
{
	public abstract class BaseUnityEventCoverter : MonoBehaviour
	{
	}

	public abstract class UnityEventConverter<TIn, TOut, TOutEvent> : BaseUnityEventCoverter
		where TOutEvent : UnityEvent<TOut>
	{
		public void Convert(TIn arg)
		{
			GetOnOutputEvent()?.Invoke(ConvertArg(arg));
		}

		protected abstract TOutEvent GetOnOutputEvent();
		protected abstract TOut ConvertArg(TIn arg);
	}

	public abstract class UnityEventConverter<TIn0, TIn1, TOut0, TOut1, TOutEvent> : BaseUnityEventCoverter
		where TOutEvent : UnityEvent<TOut0, TOut1>
	{
		public void Convert(TIn0 arg0, TIn1 arg1)
		{
			GetOutputEvent()?.Invoke(ConvertArg0(arg0), ConvertArg1(arg1));
		}

		protected abstract TOutEvent GetOutputEvent();
		protected abstract TOut0 ConvertArg0(TIn0 arg0);
		protected abstract TOut1 ConvertArg1(TIn1 arg1);
	}

	public abstract class UnityEventConverter<TIn0, TIn1, TIn2, TOut0, TOut1, TOut2, TOutEvent> : BaseUnityEventCoverter
		where TOutEvent : UnityEvent<TOut0, TOut1, TOut2>
	{
		public void Convert(TIn0 arg0, TIn1 arg1, TIn2 arg2)
		{
			GetOutputEvent()?.Invoke(ConvertArg0(arg0), ConvertArg1(arg1), ConvertArg2(arg2));
		}

		protected abstract TOutEvent GetOutputEvent();
		protected abstract TOut0 ConvertArg0(TIn0 arg0);
		protected abstract TOut1 ConvertArg1(TIn1 arg1);
		protected abstract TOut2 ConvertArg2(TIn2 arg2);
	}

	public abstract class UnityEventConverter<TIn0, TIn1, TIn2, TIn3, TOut0, TOut1, TOut2, TOut3, TOutEvent> : BaseUnityEventCoverter
		where TOutEvent : UnityEvent<TOut0, TOut1, TOut2, TOut3>
	{
		public void Convert(TIn0 arg0, TIn1 arg1, TIn2 arg2, TIn3 arg3)
		{
			GetOutputEvent()?.Invoke(ConvertArg0(arg0), ConvertArg1(arg1), ConvertArg2(arg2), ConvertArg3(arg3));
		}

		protected abstract TOutEvent GetOutputEvent();
		protected abstract TOut0 ConvertArg0(TIn0 arg0);
		protected abstract TOut1 ConvertArg1(TIn1 arg1);
		protected abstract TOut2 ConvertArg2(TIn2 arg2);
		protected abstract TOut3 ConvertArg3(TIn3 arg3);
	}
}