﻿using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.UnityEvents
{
	public abstract class BaseUnityEventProxy : MonoBehaviour
	{
	}

	public abstract class UnityEventProxy : BaseUnityEventProxy
	{
		[SerializeField] protected UnityEvent Output = null;

		public abstract void Invoke();
	}

	public abstract class UnityEventProxy<TEvent, TArg> : BaseUnityEventProxy
		where TEvent : UnityEvent<TArg>
	{
		[SerializeField] protected TEvent Output = null;

		public abstract void Invoke(TArg arg);
	}

	public abstract class UnityEventProxy<TEvent, TArg0, TArg1> : BaseUnityEventProxy
		where TEvent : UnityEvent<TArg0, TArg1>
	{
		[SerializeField] protected TEvent Output = null;

		public abstract void Invoke(TArg0 arg0, TArg1 arg1);
	}

	public abstract class UnityEventProxy<TEvent, TArg0, TArg1, TArg2> : BaseUnityEventProxy
		where TEvent : UnityEvent<TArg0, TArg1, TArg2>
	{
		[SerializeField] protected TEvent Output = null;

		public abstract void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2);
	}

	public abstract class UnityEventProxy<TEvent, TArg0, TArg1, TArg2, TArg3> : BaseUnityEventProxy
		where TEvent : UnityEvent<TArg0, TArg1, TArg2, TArg3>
	{
		[SerializeField] protected TEvent Output = null;

		public abstract void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2, TArg3 arg3);
	}
}