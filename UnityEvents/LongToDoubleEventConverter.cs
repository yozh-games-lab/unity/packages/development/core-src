﻿using System;
using UnityEngine;
using UnityEngine.Events;
using OutEvent = YOZH.Core.UnityEvents.LongToDoubleEventConverter.OutEvent;

namespace YOZH.Core.UnityEvents
{
	internal class LongToDoubleEventConverter : UnityEventConverter<long, double, LongToDoubleEventConverter.OutEvent>
	{
		[SerializeField] private OutEvent OnOutputEvent;

		protected override double ConvertArg(long arg) => (double)arg;
		protected override OutEvent GetOnOutputEvent() => OnOutputEvent;

		[Serializable] public class OutEvent : UnityEvent<double> { }
	}
}