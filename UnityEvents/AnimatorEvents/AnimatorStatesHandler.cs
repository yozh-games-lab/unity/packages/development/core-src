﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Core.UnityEvents.AnimatorEvents
{
	public partial class AnimatorStatesHandler : MonoBehaviour, IAnimatorEventBehaviour
	{
		[SerializeField] private List<AnimatorStateEvents> States = new List<AnimatorStateEvents>();

		void IAnimatorEventBehaviour.OnStateEnter(AnimatorStateInfo stateInfo, int layerIndex)
		{
			States.ForEach(state => state.RaiseOnStateEnter(stateInfo));
		}

		void IAnimatorEventBehaviour.OnStateExit(AnimatorStateInfo stateInfo, int layerIndex)
		{
			States.ForEach(state => state.RaiseOnStateExit(stateInfo));
		}

		void IAnimatorEventBehaviour.OnStateIK(AnimatorStateInfo stateInfo, int layerIndex)
		{
			States.ForEach(state => state.RaiseOnStateIK(stateInfo));
		}

		void IAnimatorEventBehaviour.OnStateMove(AnimatorStateInfo stateInfo, int layerIndex)
		{
			States.ForEach(state => state.RaiseOnStateMove(stateInfo));
		}

		void IAnimatorEventBehaviour.OnStateUpdate(AnimatorStateInfo stateInfo, int layerIndex)
		{
			States.ForEach(state => state.RaiseOnStateUpdate(stateInfo));
		}
	}
}