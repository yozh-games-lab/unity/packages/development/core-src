﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.UnityEvents.AnimatorEvents
{
	[Serializable]
	internal class AnimatorStateEvents
	{
		[SerializeField] private string StateName;
		[SerializeField] private string StateTag;

		[SerializeField] private UnityEvent OnStateEnterEvent = new UnityEvent();
		[SerializeField] private UnityEvent OnStateExitEvent = new UnityEvent();
		[SerializeField] private UnityEvent OnStateIKEvent = new UnityEvent();
		[SerializeField] private UnityEvent OnStateMoveEvent = new UnityEvent();
		[SerializeField] private UnityEvent OnStateUpdateEvent = new UnityEvent();

		public void RaiseOnStateUpdate(AnimatorStateInfo stateInfo)
		{
			if (IsTargetState(stateInfo)) {
				OnStateUpdateEvent.Invoke();
			}
		}

		public void RaiseOnStateMove(AnimatorStateInfo stateInfo)
		{
			if (IsTargetState(stateInfo)) {
				OnStateMoveEvent.Invoke();
			}
		}

		public void RaiseOnStateIK(AnimatorStateInfo stateInfo)
		{
			if (IsTargetState(stateInfo)) {
				OnStateIKEvent.Invoke();
			}
		}

		public void RaiseOnStateExit(AnimatorStateInfo stateInfo)
		{
			if (IsTargetState(stateInfo)) {
				OnStateExitEvent.Invoke();
			}
		}

		public void RaiseOnStateEnter(AnimatorStateInfo stateInfo)
		{
			if (IsTargetState(stateInfo)) {
				OnStateEnterEvent.Invoke();
			}
		}

		private bool IsTargetState(AnimatorStateInfo stateInfo)
		{
			return IsStateName(stateInfo) || IsStateTag(stateInfo);
		}

		private bool IsStateTag(AnimatorStateInfo stateInfo)
		{
			return !string.IsNullOrEmpty(StateTag) && stateInfo.IsTag(StateTag);
		}

		private bool IsStateName(AnimatorStateInfo stateInfo)
		{
			return !string.IsNullOrEmpty(StateName) && stateInfo.IsName(StateName);
		}
	}
}
