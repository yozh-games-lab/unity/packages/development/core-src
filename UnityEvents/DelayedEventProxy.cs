﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace YOZH.Core.UnityEvents
{
	public class DelayedEventProxy : UnityEventProxy
	{
		[SerializeField]
		private float Delay = 0;

		public override void Invoke()
		{
			StartCoroutine(InvokeRoutine());
		}

		private IEnumerator InvokeRoutine()
		{
			yield return new WaitForSeconds(Delay);
			Output?.Invoke();
		}

		private void OnValidate() => Delay = Mathf.Max(0, Delay);
	}

	public abstract class DelayedEventProxy<TArg> :
		UnityEventProxy<DelayedEventProxy<TArg>.Event, TArg>
	{
		[SerializeField]
		private float Delay = 0;

		public override void Invoke(TArg arg)
		{
			StartCoroutine(InvokeRoutine(arg));
		}

		private IEnumerator InvokeRoutine(TArg arg)
		{
			yield return new WaitForSeconds(Delay);
			Output?.Invoke(arg);
		}

		private void OnValidate() => Delay = Mathf.Max(0, Delay);

		[Serializable]
		public class Event : UnityEvent<TArg> { }
	}

	public abstract class DelayedEventProxy<TArg0, TArg1> :
		UnityEventProxy<DelayedEventProxy<TArg0, TArg1>.Event, TArg0, TArg1>
	{
		[SerializeField]
		private float Delay = 0;

		public override void Invoke(TArg0 arg0, TArg1 arg1)
		{
			StartCoroutine(InvokeRoutine(arg0, arg1));
		}

		private IEnumerator InvokeRoutine(TArg0 arg0, TArg1 arg1)
		{
			yield return new WaitForSeconds(Delay);
			Output?.Invoke(arg0, arg1);
		}

		private void OnValidate() => Delay = Mathf.Max(0, Delay);

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1> { }
	}

	public abstract class DelayedEventProxy<TArg0, TArg1, TArg2> :
		UnityEventProxy<DelayedEventProxy<TArg0, TArg1, TArg2>.Event, TArg0, TArg1, TArg2>
	{
		[SerializeField]
		private float Delay = 0;

		public override void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2)
		{
			StartCoroutine(InvokeRoutine(arg0, arg1, arg2));
		}

		private IEnumerator InvokeRoutine(TArg0 arg0, TArg1 arg1, TArg2 arg2)
		{
			yield return new WaitForSeconds(Delay);
			Output?.Invoke(arg0, arg1, arg2);
		}

		private void OnValidate() => Delay = Mathf.Max(0, Delay);

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1, TArg2> { }
	}

	public abstract class DelayedEventProxy<TArg0, TArg1, TArg2, TArg3> :
		UnityEventProxy<DelayedEventProxy<TArg0, TArg1, TArg2, TArg3>.Event, TArg0, TArg1, TArg2, TArg3>
	{
		[SerializeField]
		private float Delay = 0;

		public override void Invoke(TArg0 arg0, TArg1 arg1, TArg2 arg2, TArg3 arg3)
		{
			StartCoroutine(InvokeRoutine(arg0, arg1, arg2, arg3));
		}

		private IEnumerator InvokeRoutine(TArg0 arg0, TArg1 arg1, TArg2 arg2, TArg3 arg3)
		{
			yield return new WaitForSeconds(Delay);
			Output?.Invoke(arg0, arg1, arg2, arg3);
		}

		private void OnValidate() => Delay = Mathf.Max(0, Delay);

		[Serializable]
		public class Event : UnityEvent<TArg0, TArg1, TArg2, TArg3> { }
	}
}