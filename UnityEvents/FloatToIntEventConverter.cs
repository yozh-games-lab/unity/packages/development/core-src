﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.UnityEvents
{
	internal class FloatToIntEventConverter : UnityEventConverter<float, int, FloatToIntEventConverter.OutEvent>
	{
		[SerializeField] private OutEvent OnOutputEvent;
		[SerializeField] private MidpointRounding RoundingMode = MidpointRounding.AwayFromZero;

		protected override int ConvertArg(float arg)
		{
			return (int)Math.Round(arg, RoundingMode);
		}

		protected override OutEvent GetOnOutputEvent() => OnOutputEvent;

		[Serializable]
		public class OutEvent : UnityEvent<int> { }
	}
}