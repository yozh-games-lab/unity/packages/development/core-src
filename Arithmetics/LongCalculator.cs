﻿using System;
using UnityEngine;
using UnityEngine.Events;
using ResultEvent = YOZH.Core.Arithmetics.LongCalculator.ResultEvent;

namespace YOZH.Core.Arithmetics
{
	public class LongCalculator : CalculatorBehaviour<long, ResultEvent>
	{
		[SerializeField] protected ResultEvent OnResultEvent = null;

		protected override long Divide(long arg1, long arg2) => arg1 / arg2;

		protected override long Multiply(long arg1, long arg2) => arg1 * arg2;
		protected override long Substract(long arg1, long arg2) => arg1 - arg2;
		protected override long Sum(long arg1, long arg2) => arg1 + arg2;
		protected override ResultEvent GetOnResultEvent() => OnResultEvent;

		[Serializable] public class ResultEvent : UnityEvent<long> { }
	}
}