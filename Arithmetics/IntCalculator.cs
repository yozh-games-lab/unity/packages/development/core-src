﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace YOZH.Core.Arithmetics
{
	public class IntCalculator : CalculatorBehaviour<int, IntCalculator.ResultEvent>
	{
		[SerializeField] protected ResultEvent OnResultEvent = null;

		protected override int Sum(int arg1, int arg2) => arg1 + arg2;
		protected override int Substract(int arg1, int arg2) => arg1 - arg2;
		protected override int Multiply(int arg1, int arg2) => arg1 * arg2;
		protected override int Divide(int arg1, int arg2) => arg1 / arg2;

		protected override ResultEvent GetOnResultEvent() => OnResultEvent;

		[Serializable] public class ResultEvent : UnityEvent<int> { }
	}
}